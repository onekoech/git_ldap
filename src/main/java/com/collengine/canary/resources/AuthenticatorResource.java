package com.collengine.canary.resources;

//public class AuthenticatorResource {
//}

import java.util.Properties;


import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;

import com.collengine.canary.models.ResponseData;
import com.collengine.canary.models.ResponseDatas;
import com.collengine.canary.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/authenticator")
public class AuthenticatorResource {


    @Autowired
    private RestTemplate restTemplate;


    private static final String SUCCESS_RESPONSE_CODE = "4000";
    private static final String GENERIC_RESPONSE_CODE = "4001";
    private static final String EXCEPTION_RESPONSE_CODE = "4999";

    ///authenticator/auth

    @PostMapping("/auth")
    public ResponseData authenticateUser(@RequestBody User user){
        ResponseData responseData = validateAuth(user.getUserName(), user.getUserPassword());
        return responseData;
    }

    private ResponseDatas validateAuths(String username, String password) {
        ResponseDatas responseData = new ResponseDatas("user", "password");
        return responseData;
    }


    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ResponseData validateAuth(String username, String password) {

        String responseCode;
        String responseMessage;
        String errorMessage;

        try {

            /**
             * Consider migrating these configs to applications.properties file
             */

            @SuppressWarnings("unused")
            LdapContext ctx = null;
            Hashtable env = new Hashtable();
            env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
            env.put("java.naming.security.authentication", "Simple");
            env.put("java.naming.security.principal", "safaricom\\" + username);
            env.put("java.naming.security.credentials", password);
            env.put("java.naming.provider.url", "ldap://172.29.120.112:389");

            ctx = new InitialLdapContext(env, null);

            responseCode = SUCCESS_RESPONSE_CODE;
            responseMessage = "User " + username + " authenticated successfully.";
            errorMessage = "Operation successful.";

            System.out.println("Response Code : {} | Response Message {} | Error Message {} "  + responseCode + "    " + responseMessage+"         "+
                    errorMessage);

        } catch (NamingException nex) {

            /*
             * This is a fix for some certain bug noted on LDAP for some users.
             */
            if (System.err.toString()
                    .contains("LdapErr: DSID-0C0903A9, comment: AcceptSecurityContext error, data 775")) {

                responseCode = SUCCESS_RESPONSE_CODE;
                responseMessage = "User " + username + " authenticated successfully.";
                errorMessage = "Operation successful.";

            } else {

                responseCode = GENERIC_RESPONSE_CODE;
                responseMessage = "Authentication for User " + username + " failed.";
                errorMessage = "Exception : " + nex.getMessage();
            }

        } catch (Exception ex) {

            responseCode = EXCEPTION_RESPONSE_CODE;
            responseMessage = "Authentication for User " + username + " failed.";
            errorMessage = "Exception : " + ex.getMessage();

        }

        return new ResponseData(responseCode, responseMessage, errorMessage);

    }












}

